﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="POAdmin.aspx.vb" Inherits="AVPWebCustom.POAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PO EDI Administration</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>AVP PO to EDI Administration</h1>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
              <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">OrderId</th>
                      <th scope="col">OrderDate</th>
                      <th scope="col">PONumber</th>     
                      <th scope="col">ShipAfter</th>      
                      <th scope="col">Name</th>
                      <th scope="col">email</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                 <asp:Repeater ID="rPOs" runat="server">
                <ItemTemplate>
                    <tr>
                      <th scope="row"><%#DataBinder.Eval(Container.DataItem, "OrderID")%></th>
                      <td>
                          <asp:Label ID="lblCreated" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "OrderDate") %>' ></asp:Label>
                      </td>
                      <td>
                          <asp:Label ID="lblPONum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "value") %>' ></asp:Label>
                      </td>
                      <td>
                          <asp:Label ID="lblShip" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "value") %>' ></asp:Label>
                      </td>
                      <td>
                          <%#DataBinder.Eval(Container.DataItem, "DisplayName")%>
                      </td>
                      <td>
                          <%#DataBinder.Eval(Container.DataItem, "email")%>

                      </td>
                      <td>
                          <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-success" CommandName="Approve" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "OrderID")%>' />
                      </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>      
       
            </ContentTemplate>
     
        </asp:UpdatePanel>
    
    </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
