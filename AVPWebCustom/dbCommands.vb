Imports System.Data
Imports System.Data.SqlClient


'Parameter Option for Image will not accept multiple parameters if you need multiple parameters u must run a nother query

Public Class dbCommands
    Private Conn As New SqlClient.SqlConnection
    Private ConnStr As String

    Public Function Connect(ByVal ConnStr As String, Optional ByRef Message As String = "") As Boolean
        Try
            Conn.ConnectionString = ConnStr
            Conn.Open()
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try

    End Function
    Public Function DoNonQry(ByVal strQry As String, Optional ByVal sqlParams As String = "", Optional ByVal sqlParamsName As String = "", Optional ByVal BinaryData() As Byte = Nothing, Optional ByVal IsImage As Boolean = False) As String

        Dim Comm As New SqlClient.SqlCommand
        Dim tmpParams As Array
        Dim tmpParamsName As Array

        Dim i
        If sqlParamsName <> "" Then
            'check if its an image/binary data 
            If IsImage = True Then
                Comm.Parameters.Add(sqlParamsName, SqlDbType.Image)
                Comm.Parameters(sqlParamsName).Value = BinaryData

            Else
                tmpParams = Split(sqlParams, "�")
                tmpParamsName = Split(sqlParamsName, "�")
                If UBound(tmpParams) <> UBound(tmpParamsName) Then
                    Throw New ApplicationException("Invalid Parameter VS Parameter Names Count, must have the same amount of parameter names as parameters. Params " & sqlParamsName)
                    Exit Function

                End If
                For i = 0 To UBound(tmpParams)
                    If tmpParamsName(i) <> "" Then Comm.Parameters.Add(tmpParamsName(i), tmpParams(i))
                Next
            End If

        End If
        Comm.CommandText = strQry
        Comm.Connection = Conn
        Try

            Comm.ExecuteNonQuery()
            Return Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & " QRY=" & strQry)
        End Try

    End Function

    Public Sub Close()
        If Conn.State = ConnectionState.Open Then Conn.Close()

    End Sub
    Public Function DoQryDataSet(ByVal strQry As String, Optional ByVal sqlParams As String = "", Optional ByVal sqlParamsName As String = "", Optional ByVal ds1 As DataSet = Nothing, Optional ByVal tblName As String = "") As DataSet
        Dim da1 As New SqlClient.SqlDataAdapter(strQry, Conn)
        If ds1 Is Nothing Then
            ds1 = New DataSet
            tblName = "Table1"
        ElseIf tblName = "" Then
            tblName = ds1.Tables(0).TableName
        End If

        Dim tmpParams As Array
        Dim tmpParamsName As Array

        Dim i
        If sqlParamsName <> "" Then

            tmpParams = Split(sqlParams, "�")
            tmpParamsName = Split(sqlParamsName, "�")
            If UBound(tmpParams) <> UBound(tmpParamsName) Then
                ' DoQry = "Invalid Parameter VS Parameter Names Count, must have the same amount of parameter naems as parameters."
                Return Nothing

            End If
            For i = 0 To UBound(tmpParams)
                If tmpParamsName(i) <> "" Then da1.SelectCommand.Parameters.Add(tmpParamsName(i), tmpParams(i))
            Next


        End If
        da1.SelectCommand.CommandText = strQry

        Try
            da1.Fill(ds1, tblName)
        Catch ex As Exception
            Throw New Exception(ex.Message & " QRY=" & strQry)
        End Try


        Return ds1


    End Function


    Public Function DoQryDataReader(ByVal strQry As String, Optional ByVal sqlParams As String = "", Optional ByVal sqlParamsName As String = "") As SqlDataReader
        Dim Comm As New SqlClient.SqlCommand(strQry, Conn)
        Dim DR1 As SqlDataReader
        Dim tmpParams As Array
        Dim tmpParamsName As Array

        Dim i
        If sqlParamsName <> "" Then

            tmpParams = Split(sqlParams, "�")
            tmpParamsName = Split(sqlParamsName, "�")
            If UBound(tmpParams) <> UBound(tmpParamsName) Then
                ' DoQry = "Invalid Parameter VS Parameter Names Count, must have the same amount of parameter naems as parameters."
                Return Nothing

            End If
            For i = 0 To UBound(tmpParams)
                If tmpParamsName(i) <> "" Then Comm.Parameters.Add(tmpParamsName(i), tmpParams(i))
            Next


        End If
        Try
            DR1 = Comm.ExecuteReader
            Return DR1
        Catch ex As Exception
            Throw New Exception(ex.Message & " QRY=" & strQry)

        End Try



    End Function
End Class

