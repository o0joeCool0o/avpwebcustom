<%@ Page Language="VB" AutoEventWireup="false" CodeFile="POList.aspx.vb" Inherits="POList" %>

<!doctype html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AVP PO Approval List</title>    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
              <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">OrderId</th>
                      <th scope="col">PONumber</th>
                      <th scope="col">ShipDate</th>                       
                      <th scope="col">Name</th>
                      <th scope="col">email</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                 <asp:Repeater ID="rPOs" runat="server">
                <ItemTemplate>
                    <tr>
                      <th scope="row"><%#DataBinder.Eval(Container.DataItem, "OrderID")%></th>
                      <td>@<%#DataBinder.Eval(Container.DataItem, "PONumber")%></td>
                      <td>@<%#DataBinder.Eval(Container.DataItem, "ShipDate")%></td>
                      <td>@<%#DataBinder.Eval(Container.DataItem, "Name")%></td>
                      <td>@<%#DataBinder.Eval(Container.DataItem, "email")%></td>
                      <td><button type="button" class="btn btn-success">Approve</button></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>      
       
            </ContentTemplate>
     
        </asp:UpdatePanel>
    
    </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
