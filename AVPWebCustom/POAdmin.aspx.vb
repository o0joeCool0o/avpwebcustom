﻿Imports DotNetNuke.Security
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Services.Vendors
Imports System.Data
Imports DotNetNuke.Entities.Users
Public Class POAdmin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim objUserInfo As UserInfo = Nothing
            Dim intUserID As Integer = -1
            If Request.IsAuthenticated Then
                objUserInfo = UserController.Instance.GetCurrentUserInfo()
                If Not objUserInfo Is Nothing Then
                    intUserID = objUserInfo.UserID
                    If Not objUserInfo.IsInRole("Administrators") Then

                        ClientScript.RegisterStartupScript(Me.GetType(), "startUp", "<script type=text/javascript>self.parent.location='https://www.absolutevaluepublications.com/Login.aspx';</script>}")
                        Response.End()
                    End If
                End If

            Else
                ClientScript.RegisterStartupScript(Me.GetType(), "startUp", "<script type=text/javascript>self.parent.location='https://www.absolutevaluepublications.com/Login.aspx';</script>}")
                Response.End()
            End If

            bindData()

        End If
    End Sub
    Private Sub bindData()
        Dim dbcom As New dbCommands
        Dim dsOrders As New DataSet
        Dim dsAttribs As New DataSet

        Dim strQuery As String

        If dbcom.Connect(ConfigurationManager.ConnectionStrings("SiteSqlServer").ConnectionString) Then

            strQuery = "SELECT TOP(100) oa.value, o.orderid, u.DisplayName, u.email, o.OrderDate from CVStore_Orders o inner join CVStore_OrderAttributes oa on oa.parentId=o.orderid inner join Users u on u.userid=o.userid where o.orderid not in (select parentId from CVStore_OrderAttributes where name='AVP_SHIP_940') " &
                       "and oa.name='CV_STORE_ORDER_CUSTOM_PROPERTIES' and oa.value like '%PO Number%' and  o.OrderStatusID=2 order by o.OrderId desc"
            dsOrders = dbcom.DoQryDataSet(strQuery)

            rPOs.DataSource = dsOrders
            rPOs.DataBind()

            dbcom.Close()

        End If


    End Sub

    Private Sub rPOs_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rPOs.ItemDataBound
        '  ﻿<?xml version="1.0" encoding="utf-8"?><ArrayOfCheckoutCustomProperties xmlns:xsd = "http://www.w3.org/2001/XMLSchema" xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance" <> CheckoutCustomProperties <> ID > 1</Id><FieldName>PO Number</FieldName><CustomerSelectedValue>620632</CustomerSelectedValue></CheckoutCustomProperties><CheckoutCustomProperties><Id>2</Id><FieldName>Ship by</FieldName><CustomerSelectedValue /></CheckoutCustomProperties></ArrayOfCheckoutCustomProperties>
        Dim l As Label = e.Item.FindControl("lblPONum")

        If Not l Is Nothing Then
            Dim spCustomProperties As String() = l.Text.Split(New String() {"<CheckoutCustomProperties>"}, StringSplitOptions.None)
            Dim spPONum As String() = New String() {""}
            For Each split_first As String In spCustomProperties
                If split_first.Contains("PO Number&lt;/strong&gt;</FieldName><CustomerSelectedValue>") AndAlso split_first.Contains("<CustomerSelectedValue />") = False Then
                    spPONum = split_first.Split(New String() {"PO Number&lt;/strong&gt;</FieldName><CustomerSelectedValue>"}, StringSplitOptions.None)(1).Split(New String() {"</CustomerSelectedValue>"}, StringSplitOptions.None)
                End If

            Next
            l.Text = IIf(spPONum.Length > 0, spPONum(0), "")
        End If

        Dim l2 As Label = e.Item.FindControl("lblShip")
        If Not l2 Is Nothing Then
            Dim spCustomProperties As String() = l2.Text.Split(New String() {"<CheckoutCustomProperties>"}, StringSplitOptions.None)
            Dim spShipBy As String() = New String() {""}
            For Each split_first As String In spCustomProperties
                If split_first.Contains("Ship AFTER Date&lt;/strong&gt; &lt;br/&gt; (If Applicable Ex: School Closure) &lt;br /&gt; &lt;em&gt;*Leave blank to ship immediately.&lt;/em&gt;</FieldName><CustomerSelectedValue>") AndAlso split_first.Contains("<CustomerSelectedValue />") = False Then
                    spShipBy = split_first.Split(New String() {"Ship AFTER Date&lt;/strong&gt; &lt;br/&gt; (If Applicable Ex: School Closure) &lt;br /&gt; &lt;em&gt;*Leave blank to ship immediately.&lt;/em&gt;</FieldName><CustomerSelectedValue>"}, StringSplitOptions.None)(1).Split(New String() {"</CustomerSelectedValue>"}, StringSplitOptions.None)
                End If
            Next
            l2.Text = IIf(spShipBy.Length > 0, spShipBy(0), "")

        End If

    End Sub

    Private Sub rPOs_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rPOs.ItemCommand
        If e.CommandName = "Approve" Then
            Dim dbcom As New dbCommands
            Dim dsOrders As New DataSet
            Dim dsAttribs As New DataSet

            Dim strQuery As String

            If dbcom.Connect(ConfigurationManager.ConnectionStrings("SiteSqlServer").ConnectionString) Then

                strQuery = "UPDATE CVStore_Orders set OrderStatusID=20 where OrderId = @oid"
                dbcom.DoNonQry(strQuery, e.CommandArgument.ToString() & "Ä", "@oidÄ")

                dbcom.Close()
                bindData()

            End If
        End If
    End Sub
End Class